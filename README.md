# <span style="text-decoration:underline;">task fabrique.studio</span>
Project: notification_service \
SOW: https://www.craft.do/s/n6OVYFVUpq0o6L \
Developer: Yuriy IV.

## Документация по запуску проекта:

1. Создаем и активируем виртуальное окружение:

```
python -m venv venv
venv\Scripts\activate.bat 
```

2. Клонируем репозиторий:

```
git clone https://gitlab.com/iTheSand/notification_service
```

3. Переходим в папку с файлом requirements.txt и устанавливаем необходимые зависимости:

```
cd notification_service
pip install -r requirements.txt
```

4. Создаем и применяем миграции:

```
python manage.py makemigrations
python manage.py migrate
```

5. Заполняем базу данных тестовыми клиентами (DEBUG=True):

```
python manage.py filling_database 
```


6. Запускаем сервер:

```
python manage.py runserver
```

## Документация по внешнему API для интеграции с разработанным сервисом:

Для работы функционала, заложенного бизнес-логикой по ТЗ, \
создать в корне проекта файл .env, хранящий следующие переменные окружения:

```
URL = "https://probe.fbrq.cloud/v1/send/"
TOKEN = "..."
```

## Описание реализованных методов в формате OpenAPI:

Для знакомства с реализованными методами перейти по адресу:
```
http://127.0.0.1:8000/docs/
```

## Выполненные дополнительные задания:
- Сделать так, чтобы по адресу /docs/ открывалась страница со Swagger UI
и в нём отображалось описание разработанного API. Пример: https://petstore.swagger.io
- Удаленный сервис может быть недоступен, долго отвечать на запросы или выдавать
некорректные ответы. Необходимо организовать обработку ошибок и откладывание запросов 
при неуспехе для последующей повторной отправки. 
Задержки в работе внешнего сервиса никак не должны оказывать влияние на работу сервиса рассылок.