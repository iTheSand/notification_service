from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone

from mailing.models import Mailing, Client, Message
from mailing.services import send_message_on_api


@receiver(post_save, sender=Mailing)
def created_mailing(instance, created, **kwargs):
    """
    Сигнал для создания новых сообщений в бд и запуска планировщика
    """
    filter_client_properties_lst = instance.filter_client_properties.split('_')
    if len(filter_client_properties_lst) == 1:
        clients = Client.objects.filter(
            code_mobile_operator=instance.filter_client_properties).filter(
            tag='').all()
    else:
        clients = Client.objects.filter(
            code_mobile_operator=filter_client_properties_lst[0]).filter(
            tag=filter_client_properties_lst[1]).all()
    for client in clients:
        message = Message(
            mailing_id=instance,
            client_id=client
        )
        message.save()
    # запуск планировщика
    if created:
        scheduler = BackgroundScheduler()
        scheduler.start()
        if instance.check_activity:
            scheduler.add_job(
                send_message_on_api,
                trigger=CronTrigger(start_date=timezone.now(),
                                    end_date=instance.date_and_time_end),
                args=[instance.id],
                id=f"_job",
                max_instances=1,
                replace_existing=True
            )
        else:
            scheduler.add_job(
                send_message_on_api,
                trigger=CronTrigger(start_date=instance.date_and_time_start,
                                    end_date=instance.date_and_time_end),
                args=[instance.id],
                id=f"__job",
                max_instances=1,
                replace_existing=True
            )
