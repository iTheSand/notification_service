from django.core.validators import RegexValidator
from django.db import models
from django.utils import timezone


class Mailing(models.Model):
    date_and_time_start = models.DateTimeField(verbose_name='дата и время запуска рассылки')
    text_message = models.CharField(max_length=256, verbose_name='текст сообщения для доставки клиенту')
    filter_client_properties_regex = RegexValidator(regex=r'^9\d{2}\w{0,33}$',
                                                    message='Введите код моб. оператора и тег через нижнее подчеркивание')
    filter_client_properties = models.CharField(validators=[filter_client_properties_regex], max_length=36,
                                                verbose_name='фильтр по коду моб. оператора и тегу')
    date_and_time_end = models.DateTimeField(verbose_name='дата и время окончания рассылки')

    @property
    def check_activity(self):
        if self.date_and_time_start <= timezone.now() <= self.date_and_time_end:
            return True
        else:
            return False

    def __str__(self):
        return f'Рассылка - {self.id}, содержание - {self.text_message}'


class Client(models.Model):
    phone_regex = RegexValidator(regex=r'^7\d{10}$',
                                 message='Введите номер в следующем формате: 7XXXXXXXXXX (где Х - цифра от 0 до 9)')
    phone_number = models.PositiveIntegerField(validators=[phone_regex], unique=True, blank=False,
                                               verbose_name='номер телефона')
    code_mobile_operator_regex = RegexValidator(regex=r'^9\d{2}$',
                                                message='Введите код моб. оператора в следующем формате: 9XX (где Х - цифра от 0 до 9)')
    code_mobile_operator = models.CharField(validators=[code_mobile_operator_regex], max_length=3, blank=False,
                                            verbose_name='код мобильного оператора')
    tag = models.CharField(max_length=32, blank=True, verbose_name='произвольная метка')
    time_zone = models.CharField(max_length=6, default='+03:00', verbose_name='часовой пояс')

    def __str__(self):
        return f'Клиент - {self.id}, номер телефона {self.phone_number}, тэг {self.tag}'


class Message(models.Model):
    NEW = 'N'
    SENT = 'S'
    FAIL = 'F'

    STATUS_CHOICES = (
        (NEW, 'новое'),
        (SENT, 'отправлено'),
        (FAIL, 'неудачно')
    )

    date_and_time_sending = models.DateTimeField(null=True, blank=True, verbose_name='дата и время отправки сообщения')
    status_sending = models.CharField(max_length=1, default='N', choices=STATUS_CHOICES, verbose_name='статус отправки')
    mailing_id = models.ForeignKey(Mailing, on_delete=models.CASCADE, verbose_name='id рассылки',
                                   related_name='messages')
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name='id клиента')

    def __str__(self):
        status_verbose_names = {'N': 'новое',
                                'S': 'отправлено',
                                'F': 'неудачно'}
        return f'Сообщение - {self.id}, {self.client_id}, статус {status_verbose_names[self.status_sending]}'
