from django.core.management import BaseCommand

from progress.bar import IncrementalBar
from mailing.models import Client
from mimesis import Person


class Command(BaseCommand):
    help = 'Заполнение телефонного справочника клиентами'

    def handle(self, *args, **options):
        # СОЗДАНИЕ КЛИЕНТОВ
        person = Person()
        print('Добавление клиентов в телефонный справочник')
        # устанавливаем размер телефонного каталога
        size_phone_directory = range(1000)
        # визуализация процесса заполнения каталога
        bar = IncrementalBar('Выполнение:', max=len(size_phone_directory))
        for _ in size_phone_directory:
            phone_number_ = person.telephone(mask='79#########')
            code_mobile_operator_ = phone_number_[1:4]
            # добавляем нового клиента
            client = Client(
                phone_number=int(phone_number_),
                code_mobile_operator=code_mobile_operator_,
            )
            # сохраняем нового клиента
            client.save()
            bar.next()
        bar.finish()
        print('Добавление клиентов в телефонный справочник завершено!')
