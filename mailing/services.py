import os
import requests
from django.utils import timezone
from dotenv import load_dotenv

from mailing.models import Mailing
from .models import Message

load_dotenv()
URL = os.getenv("URL")
TOKEN = os.getenv("TOKEN")


def send_message_on_api(mail_id, url=URL, token=TOKEN):
    """
    Отправка сообщений на API внешнего сервиса
    """
    # формирование заголовка
    header = {
        'Authorization': f'Bearer {token}',
        'Content-Type': 'application/json'}
    # текущая рассылка
    mailing = Mailing.objects.filter(id=mail_id).first()
    # достаем все новые и не отправленные сообщения
    messages = Message.objects.filter(mailing_id=mailing).filter(status_sending='N' or 'F').all()
    for message in messages:
        # формируемый json для отправки
        data = {
            'id': message.id,
            "phone": message.client_id.phone_number,
            "text": message.mailing_id.text_message
        }
        # запрос на API внешнего сервиса
        try:
            response = requests.post(url=url + str(message.id),
                                     headers=header,
                                     json=data)
            print(f'Ответ от сервера - {response.status_code}')
            # меняем статусы у отправленных/не отправленных сообщений
            if response.status_code == 200:
                message.status_sending = 'S'
                message.date_and_time_sending = timezone.now()
            else:
                message.status_sending = 'F'
            message.save()
        except ConnectionError as err:
            return err
