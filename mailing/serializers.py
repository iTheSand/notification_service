from rest_framework.serializers import ModelSerializer, StringRelatedField
from rest_framework.fields import ReadOnlyField
from .models import Mailing, Client, Message


class MailingSerializer(ModelSerializer):
    """
    Сериализатор для рассылки
    """
    class Meta:
        model = Mailing
        fields = ['id', 'date_and_time_start', 'text_message',
                  'filter_client_properties', 'date_and_time_end']


class MailingDetailInfoSerializer(ModelSerializer):
    """
    Сериализатор для детализации конкретной рассылки
    """
    messages = StringRelatedField(many=True)

    class Meta:
        model = Mailing
        fields = ['id', 'date_and_time_start', 'text_message',
                  'filter_client_properties', 'date_and_time_end', 'messages']


class MailingFullInfoSerializer(ModelSerializer):
    """
    Сериализатор для общей статистики созданных рассылок и кол-ва отправленных сообщений
    """
    count_messages_sent = ReadOnlyField(source='messages.count')

    class Meta:
        model = Mailing
        fields = ['id', 'date_and_time_start', 'text_message',
                  'filter_client_properties', 'date_and_time_end', 'count_messages_sent']


class ClientSerializer(ModelSerializer):
    """
    Сериализатор для клиентов
    """
    class Meta:
        model = Client
        fields = ['id', 'phone_number', 'code_mobile_operator', 'tag', 'time_zone']


class MessageSerializer(ModelSerializer):
    """
    Сериализатор для сообщений
    """
    class Meta:
        model = Message
        fields = ['id', 'date_and_time_sending', 'status_sending', 'mailing_id', 'client_id']
