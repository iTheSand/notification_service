from django.shortcuts import get_object_or_404
from rest_framework.pagination import LimitOffsetPagination
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.viewsets import ModelViewSet
from mailing.models import Mailing, Client, Message
from mailing.serializers import MailingSerializer, ClientSerializer, MessageSerializer, \
    MailingDetailInfoSerializer, MailingFullInfoSerializer


# Create your views here.
class MailingViewSet(ModelViewSet):
    """
    Представление для рассылок
    """
    serializer_class = MailingSerializer
    queryset = Mailing.objects.all()

    @action(methods=['GET'], detail=True)
    def detail_info(self, request, pk=None):
        """
        Детальная статистика сообщений по конкретной рассылке
        """
        mailing = get_object_or_404(Mailing, pk=pk)
        serializer = MailingDetailInfoSerializer(mailing)
        return Response(serializer.data)

    @action(methods=['GET'], detail=False)
    def full_info(self, request):
        """
        Общая статистика по созданным рассылкам и количеству отправленных сообщений
        """
        queryset = Mailing.objects.all()
        serializer = MailingFullInfoSerializer(queryset, many=True)
        return Response(serializer.data)


class ClientPagination(LimitOffsetPagination):
    """
    Пагинация для клиентов
    """
    default_limit = 20


class ClientViewSet(ModelViewSet):
    """
    Представление для клиентов
    """
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    pagination_class = ClientPagination


class MessagePagination(LimitOffsetPagination):
    """
    Пагинация для сообщений
    """
    default_limit = 20


class MessageViewSet(ModelViewSet):
    """
    Представление для сообщений
    """
    serializer_class = MessageSerializer
    queryset = Message.objects.all()
    pagination_class = MessagePagination
